ENV["NOTURN"] = "1"
# elasticsearch-dsl is not compatible with test-unit 3
modules = ['elasticsearch', 'elasticsearch-transport', 'elasticsearch-api',
           'elasticsearch-extensions','elasticsearch-watcher']
$: << "."
modules.each { |m| $: << "#{m}/lib" }
modules.each do |m|
  $: << "#{m}/test"
  Dir["#{m}/test/unit/*.rb"].each { |f| require f }
  $:.pop
end
